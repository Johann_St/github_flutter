# test_app

test app github users

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

Description

Need to create an application which will display a list of Github users.
User should be able to filter users by the nickname.
Github users should be divided by the first nickname letter (started from A-H, started from I-P and started from Q-Z) and should be placed into three separated lists (users will switch between lists by swiping left and right or pressing tabs).
Each user should be displayed as a list item, which contains avatar, name, count of followers and count of following users.

Sketch attached.

Requirements
- Clean and responsive interface
- Good structured code
- Project should use BLoC architecture
