import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/repo/github_cache.dart';
import 'package:test_app/repo/github_client.dart';
import 'package:test_app/repo/github_repository.dart';

import 'bloc/github_search_bloc.dart';
import 'widgets/my_home_page.dart';

void main() {
  final GithubRepository _githubRepository = GithubRepository(
    GithubCache(),
    GithubClient(),
  );

  runApp(MyApp(githubRepository: _githubRepository));
}

class MyApp extends StatelessWidget {
  final GithubRepository githubRepository;

  const MyApp({
    Key key,
    @required this.githubRepository,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Github users search",
      initialRoute: '/',
      routes: {
        '/': (context) => BlocProvider(
              create: (context) =>
                  GithubSearchBloc(githubRepository: githubRepository),
              child: MyHomePage(githubRepository: githubRepository),
            ),
      },
      theme: ThemeData(primarySwatch: Colors.blue),
    );
  }
}
