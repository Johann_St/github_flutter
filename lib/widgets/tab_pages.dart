import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:test_app/model/search_result_item.dart';
import 'package:test_app/utils/constants.dart';
import 'package:url_launcher/url_launcher.dart';

class UsersInInterval extends StatefulWidget {
  final List<SearchResultItem> items;

  const UsersInInterval({Key key, this.items}) : super(key: key);

  @override
  State<StatefulWidget> createState() => UsersInIntervalState();
}

class UsersInIntervalState extends State<UsersInInterval> {
  @override
  Widget build(BuildContext context) {
    return widget.items.isNotEmpty
        ? ListView.builder(
            itemCount: widget.items.length,
            itemBuilder: (BuildContext context, int index) {
              return _SearchResultItem(item: widget.items[index]);
            },
          )
        : Center(
            child: Text(NO_RESULTS_LOOK_TO_ANOTHER_COLUMN),
          );
  }

  @override
  void initState() {
    super.initState();
  }
}

class _SearchResultItem extends StatefulWidget {
  final SearchResultItem item;

  const _SearchResultItem({Key key, @required this.item}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SearchResultItemState();
}

class _SearchResultItemState extends State<_SearchResultItem> {
  Future<SearchResultItem> fetchFollowDataFuture;
  bool _isFetchFollowData = false;

  @override
  Widget build(BuildContext context) {
    var _followData = _isFetchFollowData
        ? getTextForUser(widget.item)
        : getStubForFollowData(widget.item);
    return ListTile(
      leading: CircleAvatar(
        child: Image.network(widget.item.owner.avatarUrl),
      ),
      title: _followData,
      onTap: () async {
        if (await canLaunch(widget.item.htmlUrl)) {
          await launch(widget.item.htmlUrl);
        }
      },
    );
  }

  @override
  void initState() {
    super.initState();
    fetchFollowDataFuture = downloadFollowData();
  }

  Future<SearchResultItem> downloadFollowData() async {
    if (!widget.item.owner.isFollowDataSet()) {
      if (!widget.item.owner.isFetchingData) {
        await widget.item.owner.fetchFollowData();
        if (mounted) {
          setState(() {
            _isFetchFollowData = true;
          });
        }
      }
    } else {
      _isFetchFollowData = true;
    }
    return widget.item;
  }
}

Widget getStubForFollowData(SearchResultItem item) {
  return Wrap(
    crossAxisAlignment: WrapCrossAlignment.center,
    children: [
      Text(item.owner.login + " "),
      RefreshProgressIndicator(),
      Text(' / '),
      RefreshProgressIndicator(),
    ],
  );
}

Widget getTextForUser(SearchResultItem item) {
  return Text(item.owner.login +
      "  " +
      item.owner.followersCount +
      " " +
      "/" +
      " " +
      item.owner.followingCount);
}
