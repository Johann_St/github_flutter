import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_search_bar/flutter_search_bar.dart';
import 'package:test_app/bloc/github_search_bloc.dart';
import 'package:test_app/bloc/github_search_event.dart';
import 'package:test_app/bloc/github_search_state.dart';
import 'package:test_app/repo/github_repository.dart';
import 'package:test_app/utils/constants.dart';
import 'package:test_app/utils/error_utils.dart';

import 'tab_pages.dart';

class MyHomePage extends StatefulWidget {
  final GithubRepository githubRepository;

  const MyHomePage({
    Key key,
    @required this.githubRepository,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() =>
      _MyHomePageState(this.githubRepository);
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  SearchBar searchBar;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GithubRepository githubRepository;
  GithubSearchBloc _githubSearchBloc;
  TabController _tabController;

  @override
  void initState() {
    _githubSearchBloc = BlocProvider.of<GithubSearchBloc>(context);
    _tabController = TabController(length: TAB_CONTROLLER_LENGTH, vsync: this);
    super.initState();
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
        title: Text("Github users search"),
        actions: [searchBar.getSearchAction(context)]);
  }

  void onSubmitted(String value) {
    setState(() {
      _githubSearchBloc.add(SearchStart(value));
    });
  }

  _MyHomePageState(this.githubRepository) {
    searchBar = SearchBar(
        inBar: false,
        buildDefaultAppBar: buildAppBar,
        setState: setState,
        onSubmitted: onSubmitted,
        onCleared: () {
          print("cleared");
        },
        onClosed: () {
          print("closed");
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: searchBar.build(context),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Material(
                color: Colors.lightBlue,
                child: TabBar(
                  unselectedLabelColor: Colors.black26,
                  labelColor: Colors.black,
                  tabs: myTabs,
                  controller: _tabController,
                  indicatorSize: TabBarIndicatorSize.tab,
                )),
            BlocBuilder<GithubSearchBloc, GithubSearchState>(
              builder: (context, state) {
                if (state is SearchStateLoading) {
                  return const CircularProgressIndicator();
                } else if (state is SearchStateInactive) {
                  return const Text(START_SEARCH);
                } else if (state is SearchStateError) {
                  return Text(state.error);
                } else if (state is SearchStateSuccess) {
                  return state.isEmpty()
                      ? const Text(NO_RESULTS)
                      : Expanded(
                          child: TabBarView(
                          children: [
                            UsersInInterval(items: state.itemsA),
                            UsersInInterval(items: state.itemsI),
                            UsersInInterval(items: state.itemsQ),
                          ],
                          controller: _tabController,
                        ));
                } else
                  return const Text(START_SEARCH);
              },
            )
          ],
        ),
      ),
    );
  }

  final List<Widget> myTabs = <Widget>[
    Container(
        color: Colors.lightBlue,
        alignment: Alignment.center,
        child: Tab(text: A_H)),
    Container(
      color: Colors.lightBlue,
      alignment: Alignment.center,
      child: Tab(text: I_P),
    ),
    Container(
      color: Colors.lightBlue,
      alignment: Alignment.center,
      child: Tab(text: Q_Z),
    ),
  ];
}
