import 'dart:convert';

import 'package:test_app/repo/github_client.dart';
import 'package:test_app/utils/constants.dart';
import 'package:test_app/utils/error_utils.dart';

class GithubUser {
  final String login;
  final String avatarUrl;
  final String followersUrl;
  final String followingUrl;
  String followersCount = REQUEST_FAIL;
  String followingCount = REQUEST_FAIL;
  bool isFollowersSet = false;
  bool isFollowingSet = false;
  bool isFetchingData = false;
  GithubClient httpClient;

  GithubUser(
      {this.login,
      this.avatarUrl,
      this.followersUrl,
      this.followingUrl,
      this.httpClient});

  static GithubUser fromJson(dynamic json, GithubClient httpClient) {
    return GithubUser(
        login: json['login'] as String,
        avatarUrl: json['avatar_url'] as String,
        followersUrl: getUrl(json['followers_url'] as String),
        followingUrl: getUrl(json['following_url'] as String),
        httpClient: httpClient);
  }

  Future<void> setFollowers(GithubClient httpClient) async {
    try {
      await fetchFollowersQuick(httpClient, true);
    } catch (error) {
      handleError(error);
    }
    try {
      await fetchFollowersQuick(httpClient, false);
    } catch (error) {
      handleError(error);
    }
  }

  fetchFollowersQuick(GithubClient client, bool isFollowers) async {
    isFetchingData = true;
    int i = 1;
    int upBorder = 1;
    int downBorder = 1;
    int length = PER_PAGE;
    int result;
    bool up = true;
    while (length == PER_PAGE) {
      length = await fetchFollowOnPage(client, isFollowers, i);
      if (length > 0 && length < PER_PAGE) {
        length = (i - 1) * PER_PAGE + length;
      } else if (length == 0 && i > 2) {
        length = await fetchFollowOnPage(client, isFollowers, i - 1);
        if (length == 0) {
          upBorder = i;
          up = false;
          length = PER_PAGE;
          i = (i - ((upBorder - downBorder) / 2).round());
        } else {
          length = (i - 2) * PER_PAGE + length;
        }
      } else {
        if (i > AVERAGE_NUMBER_PAGE_FOLLOWERS) {
          if (up) {
            i *= 2;
            upBorder = i;
          } else {
            downBorder = i;
            i = i + ((upBorder - i) / 2).round();
          }
        } else {
          if (length == 0 && i <= AVERAGE_NUMBER_PAGE_FOLLOWERS) {
            length = (i - 1) * 100;
            break;
          }
          i++;
        }
      }
    }
    result = length;
    if (isFollowers) {
      followersCount = result.toString();
      isFollowersSet = true;
    } else {
      followingCount = result.toString();
      isFollowingSet = true;
    }
    isFetchingData = false;
  }

  Future<int> fetchFollowOnPage(
      GithubClient client, bool isFollowers, int i) async {
    String tmp = isFollowers ? followersUrl : followingUrl;
    final responseFollowers = await client.fetchData(tmp + i.toString());
    final results = json.decode(responseFollowers.body);
    if (responseFollowers.statusCode == 200) {
      var length = getLength(results);
      return length;
    } else {
      throw SearchResultError.fromJson(results);
    }
  }

  static String getUrl(String url) {
    if (url.contains("{")) {
      var indexOf = url.indexOf("{");
      url = url.substring(0, indexOf);
    }
    return url + "?per_page=100&page=";
  }

  static int getLength(List<dynamic> json) {
    return json.length;
  }

  Future<void> fetchFollowData() async {
    await setFollowers(httpClient);
  }

  bool isFollowDataSet() {
    return isFollowingSet && isFollowersSet;
  }
}
