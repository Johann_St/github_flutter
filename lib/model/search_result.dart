import 'dart:async';

import 'package:test_app/repo/github_client.dart';

import 'search_result_item.dart';

class SearchResult {
  final List<SearchResultItem> itemsA;
  final List<SearchResultItem> itemsI;
  final List<SearchResultItem> itemsQ;

  const SearchResult({this.itemsA, this.itemsI, this.itemsQ});

  static Future<SearchResult> fromJson(
      List<dynamic> json, GithubClient httpClient) async {
    List<SearchResultItem> allResultsList = List();
    for (final i in json) {
      var fromJson =
          SearchResultItem.fromJson(i as Map<String, dynamic>, httpClient);
      var searchResultItem = await fromJson;
      allResultsList.add(searchResultItem);
    }
    allResultsList.sort((a, b) =>
        a.owner.login.toLowerCase().compareTo(b.owner.login.toLowerCase()));
    List<SearchResultItem> aList = new List();
    List<SearchResultItem> iList = new List();
    List<SearchResultItem> qList = new List();
    fillListsByOwnersLoginFirstLetter(allResultsList, aList, iList, qList);

    return SearchResult(itemsA: aList, itemsI: iList, itemsQ: qList);
  }

  static void fillListsByOwnersLoginFirstLetter(
      List<SearchResultItem> mainList,
      List<SearchResultItem> listA,
      List<SearchResultItem> listI,
      List<SearchResultItem> listQ) {
    //^[a-h]
    RegExp expA = new RegExp(r"(^[a-h])");
    RegExp expI = new RegExp(r"(^[i-p])");
    RegExp expQ = new RegExp(r"(^[q-z])");
    while (true) {
      if (mainList.length > 0) {
        var first = mainList.first;
        var login = first.owner.login;
        if (expA.hasMatch(login)) {
          listA.add(first);
        } else if (expI.hasMatch(login)) {
          listI.add(first);
        } else if (expQ.hasMatch(login)) {
          listQ.add(first);
        }
        mainList.remove(first);
      } else
        break;
    }
  }
}
