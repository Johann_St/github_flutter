import 'package:test_app/utils/time_utils.dart';

class RateLimitResult {
  final RateItem item;

  const RateLimitResult({this.item});

  static RateLimitResult fromJson(Map<String, dynamic> json) {
    final item = RateItem.fromJson(json['search']);
    return RateLimitResult(item: item);
  }
}

class RateItem {
  final int searchLimit;
  final int searchLimitUsed;
  final int searchLimitRemaining;
  final String searchLimitReset;

  RateItem(
      {this.searchLimit,
      this.searchLimitUsed,
      this.searchLimitRemaining,
      this.searchLimitReset});

  static RateItem fromJson(dynamic json) {
    return RateItem(
        searchLimit: json["limit"] as int,
        searchLimitUsed: json["used"] as int,
        searchLimitRemaining: json["remaining"] as int,
        searchLimitReset: getDateTimeFromEpoch(json["reset"] as int));
  }

  @override
  String toString() {
    return 'searchLimit: $searchLimit \n, searchLimitUsed: $searchLimitUsed \n,'
        ' searchLimitRemaining: $searchLimitRemaining \n,'
        ' searchLimitReset: $searchLimitReset}';
  }
}
