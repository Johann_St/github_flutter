import 'package:test_app/repo/github_client.dart';

import 'github_user.dart';

class SearchResultItem {
  final GithubUser owner;
  final String htmlUrl;

  const SearchResultItem({this.owner, this.htmlUrl});

  static Future<SearchResultItem> fromJson(
      dynamic json, GithubClient httpClient) async {
    var searchResultItem = SearchResultItem(
      owner: GithubUser.fromJson(json, httpClient),
      htmlUrl: json["html_url"] as String,
    );
    return searchResultItem;
  }

  @override
  String toString() {
    return 'SearchResultItem{owner: $owner, htmlUrl: $htmlUrl}';
  }
}
