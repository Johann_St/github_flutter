import 'package:intl/intl.dart';

String getDateTimeFromEpoch(int timeInMillis) {
  var date = DateTime.fromMillisecondsSinceEpoch(timeInMillis*1000);
  var formattedDate = DateFormat.yMMMd().format(date);
  var formattedDate2 = DateFormat.Hms().format(date);
  return formattedDate + " " + formattedDate2;
}
String getDateTimeFromString(String timeInMillis) {
  var tryParse = int.tryParse(timeInMillis);
  var date = DateTime.fromMillisecondsSinceEpoch(tryParse);
  var formattedDate = DateFormat.yMMMd().format(date);
  return formattedDate;
}