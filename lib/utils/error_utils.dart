import 'package:test_app/bloc/github_search_state.dart';

void handleError(dynamic error) {
  if (error is SearchResultError) {
    print(error.message);
  }
  print(error.toString());
}

 class SearchResultError {
  final String message;

  const SearchResultError({this.message});

  static SearchResultError fromJson(dynamic json) {
    return SearchResultError(
      message: json['message'] as String,
    );
  }
}
class SearchStateError extends GithubSearchState {
  final String error;

  const SearchStateError(this.error);

  @override
  List<Object> get props => [error];
}
