const int PER_PAGE = 100;
const int API_SEARCH_LIMIT_PER_QUERY = 10;
const int AVERAGE_NUMBER_PAGE_FOLLOWERS = 5;
const int TAB_CONTROLLER_LENGTH = 3;
const int POPULAR_USER_PAGES = 5;
const String NO_RESULTS = 'No Results';
const String NO_RESULTS_LOOK_TO_ANOTHER_COLUMN =
    'No results, maybe they are available in another column';
const String START_SEARCH = "Please enter a user nickname or its part to begin";
const String A_H = "A-H";
const String I_P = "I-P";
const String Q_Z = "Q-Z";
const String REQUEST_FAIL = "failed request";
const String API_RESULTS_AVAILABLE =
    "Only the first 1000 search results are available";
const String OS_ERROR = "Connection lost. Check the internet connection";
