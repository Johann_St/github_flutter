import 'package:test_app/model/search_result.dart';

class GithubCache {
  final _cache = <String, SearchResult>{};

  SearchResult get(String savedRequest) => _cache[savedRequest];

  void set(String savedRequest, SearchResult result) =>
      _cache[savedRequest] = result;

  bool contains(String savedRequest) => _cache.containsKey(savedRequest);
}
