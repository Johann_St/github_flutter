import 'dart:async';


import 'package:test_app/model/search_result.dart';

import 'github_cache.dart';
import 'github_client.dart';


class GithubRepository {
  final GithubCache cache;
  final GithubClient client;

  GithubRepository(this.cache, this.client);

  Future<SearchResult> search(String searchRequest) async {
    if (cache.contains(searchRequest)) {
      return cache.get(searchRequest);
    } else {
      await client.fetchRateData();
      final result = await client.search(searchRequest);
      cache.set(searchRequest, result);
      return result;
    }
  }
}
