import 'dart:async';
import 'dart:convert';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:test_app/model/rate_limit_result.dart';
import 'package:test_app/model/search_result.dart';
import 'package:test_app/utils/constants.dart';
import 'package:test_app/utils/error_utils.dart';

class GithubClient {
  final String baseUrl;
  final http.Client httpClient;

  Map<String, String> get headers => {
        "HttpHeaders.contentTypeHeader": "application/json",
        "Accept": "application/json",
        "Authorization": "token 1646a3959a09a7a89d3abbf07c60f2110bdd902c",
      };

  GithubClient({
    http.Client httpClient,
    this.baseUrl = "http://api.github.com/",
  }) : this.httpClient = httpClient ?? http.Client();

  Future<SearchResult> search(String term) async {
    String query =
        "search/users?q=$term+in:name+type%3Auser&per_page=100&page=";
    int i = 0;
    List<dynamic> resultList = List();
    while (true) {
      i++;
      if (i > API_SEARCH_LIMIT_PER_QUERY) {
        Fluttertoast.showToast(
          msg: API_RESULTS_AVAILABLE,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
        );
        break;
      }
      final response = await fetchData("$baseUrl$query$i");
      final results = json.decode(response.body);
      if (response.statusCode == 200) {
        var result = results["items"];
        var length = getLength(result);
        if (length > 0) {
          resultList.addAll(result);
        } else {
          break;
        }
      } else if (response.statusCode == 403) {
        Fluttertoast.showToast(
          msg: "API RATE LIMIT EXCEEDED",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
        );
        fetchRateData();
        break;
      } else if (response.statusCode == 422) {
        break;
      } else {
        throw SearchResultError.fromJson(results);
      }
    }
    return await SearchResult.fromJson(resultList, this);
  }

  Future<void> fetchRateData() async {
    String queryUser = "rate_limit";
    final userResponse = await fetchData("$baseUrl$queryUser");
    final userResults = json.decode(userResponse.body);
    var fromJson = RateLimitResult.fromJson(userResults["resources"]);
    if (userResponse.statusCode == 200) {
      Fluttertoast.showToast(
        msg: fromJson.item.toString(),
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
      );
    } else {}
  }

  Future<Response> fetchData(String url) async {
    return httpClient.get(Uri.parse(url), headers: headers);
  }

  static int getLength(List<dynamic> json) {
    return json.length;
  }
}
