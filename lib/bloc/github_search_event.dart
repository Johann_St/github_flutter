import 'package:equatable/equatable.dart';

abstract class GithubSearchEvent extends Equatable {
  const GithubSearchEvent();
}

class SearchStart extends GithubSearchEvent {
  final String finalText;
  const SearchStart(this.finalText);
  @override
  List<Object> get props => [finalText];
  @override
  String toString() => 'SearchStart { request: $finalText }';
}