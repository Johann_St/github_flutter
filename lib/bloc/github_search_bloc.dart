import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:meta/meta.dart';
import 'package:test_app/model/search_result.dart';
import 'package:test_app/repo/github_repository.dart';
import 'package:test_app/utils/constants.dart';
import 'package:test_app/utils/error_utils.dart';

import 'github_search_event.dart';
import 'github_search_state.dart';

class GithubSearchBloc extends Bloc<GithubSearchEvent, GithubSearchState> {
  final GithubRepository githubRepository;

  GithubSearchBloc({@required this.githubRepository})
      : super(SearchStateInactive());

  @override
  Stream<GithubSearchState> mapEventToState(GithubSearchEvent event) async* {
    if (event is SearchStart) {
      final String searchTerm = event.finalText;
      if (searchTerm.isEmpty) {
        yield SearchStateEmpty();
      } else {
        yield SearchStateLoading();
        try {
          final results = await githubRepository.search(searchTerm);
          yield SearchStateSuccess(results.itemsA, results.itemsI,
              results.itemsQ, getResultLength(results));
        } catch (error) {
          yield error is SearchResultError
              ? SearchStateError(error.message)
              : error is SocketException
                  ? handleSocketException(error)
                  : SearchStateError(error.toString());
        }
      }
    }
  }

  int getResultLength(SearchResult result) {
    return result.itemsA.length + result.itemsI.length + result.itemsQ.length;
  }

  handleSocketException(SocketException error) {
    Fluttertoast.showToast(
      msg: OS_ERROR,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.CENTER,
    );
    SearchStateError(error.toString());
  }
}
