import 'package:equatable/equatable.dart';
import 'package:test_app/model/search_result_item.dart';

abstract class GithubSearchState extends Equatable {
  const GithubSearchState();

  @override
  List<Object> get props => [];
}

class SearchStateEmpty extends GithubSearchState {}

class SearchStateLoading extends GithubSearchState {}

class SearchStateInactive extends GithubSearchState {}

class SearchStateSuccess extends GithubSearchState {
  final List<SearchResultItem> itemsA;
  final List<SearchResultItem> itemsI;
  final List<SearchResultItem> itemsQ;
  final int resultLength;

  const SearchStateSuccess(
      this.itemsA, this.itemsI, this.itemsQ, this.resultLength);

  @override
  List<Object> get props => [itemsA, itemsI, itemsQ];

  @override
  String toString() => 'SearchStateSuccess { items: ' '$resultLength }';

  bool isEmpty() {
    return resultLength == 0;
  }
}

